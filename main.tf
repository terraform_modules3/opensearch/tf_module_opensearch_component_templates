locals {
  files = fileset(var.component_templates_dir, "*.json")
}

resource "opensearch_component_template" "component_template" {
  for_each = { for file in local.files : trimsuffix(file, ".json") => file }

  name = each.key
  body = file("${var.component_templates_dir}/${each.value}")
}
