variable "component_templates_dir" {
  description = <<EOT
  Path to dir containing component_tempates definintions in .json files

  Example dir structure:
  component_templates
  ├── fields_datatypes.json
  ├── fields_dynamic_mappings.json
  └── disable_object_indexing.json

  Name of a file is used as a name of component_tempate

  EOT
  type        = string
  default     = "./component_templates"
}
