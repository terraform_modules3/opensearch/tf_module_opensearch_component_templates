# Terraform Opensearch Component Templates Module
Creates Opensearch component templates

## Usage example
```hcl
# main.tf
module "component_template" {
  source = "git::https://gitlab.com/terraform_modules3/opensearch/tf_module_opensearch_component_templates.git?ref=0.1.0"
}
```
```
.
├── main.tf
└── component_templates
    ├── fields_datatypes.json
    ├── disable_object_indexing.json
    └── fields_dynamic_mappings.json
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_opensearch"></a> [opensearch](#requirement\_opensearch) | >= 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_opensearch"></a> [opensearch](#provider\_opensearch) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [opensearch_component_template.component_template](https://registry.terraform.io/providers/opensearch-project/opensearch/latest/docs/resources/component_template) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_component_templates_dir"></a> [component\_templates\_dir](#input\_component\_templates\_dir) | Path to dir containing component\_tempates definintions in .json files<br><br>  Example dir structure:<br>  component\_templates<br>  ├── fields\_datatypes.json<br>  ├── fields\_dynamic\_mappings.json<br>  └── disable\_object\_indexing.json<br><br>  Name of a file is used as a name of component\_tempate | `string` | `"./component_templates"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
